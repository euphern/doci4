<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}
class Webservice extends CI_Controller {
	public function __construct() {
		parent::__construct();
		error_reporting(0);
		date_default_timezone_set('Asia/Kolkata');		
		$this->load->model('Webservice_model','webservice_model');	
		$this->load->model('Home_Model');	
		$this->load->library('form_validation');
	}
	public function index(){
		echo "hi! guys";
	}
	public function appkey_validator(){		
		$data =json_decode(file_get_contents('php://input'), true);	
		$result = $this->webservice_model->check_app_key($data['my_key']);
		echo $result;
	}
	public function Signin(){	
		$data =json_decode(file_get_contents('php://input'), true);	
		if(isset($data) && !empty($data)){
			$result = $this->webservice_model->signin($data);
			echo json_encode($result);
		}
	}
	public function Signup(){		
		$data =json_decode(file_get_contents('php://input'), true);
		if(isset($data) && !empty($data)){
			if($this->Isemailexist($data['email'])){
				$data = array('status'=>false,'msg'=>'We already have this email address');
			}else{
				$password = md5($data['password']);	
				$str_passw = $data['password'];
				unset($data['password']);				
				$result = $this->webservice_model->signup($data,$password);
				if($result){
					$data['password']= $str_passw;
					$x = $this->Home_Model->sendEmail($data);
				unset($data['password']);				
					$data = array('status'=>true,'msg'=>'Successfully Registered. Email sent to your credentials. Please login to your account');
				}
			}
			echo json_encode($data);				
		}		
	}
	public function Isemailexist($email){
		$doctor_exist = $this->webservice_model->isEmailExistPatient($email);	
    if ($doctor_exist) {		
        return false;
		}else {
			return true;
		}	
	}
    public function Final_return($status,$error,$message) {
        if($status == "success") {
            $ret_msg = array('status'=>"success",'message' => $message);
        } else {
            $ret_msg = array('status'=>"error",'error' => $error,'message' => $message);    
        }
        header('Content-type: application/json');
        return json_encode($ret_msg);
    }
	public function Forget_password(){
		if($_POST){
			if(isset($_POST) && !empty($_POST)){
				$data =json_decode(file_get_contents('php://input'), true);
				$result = $this->webservice_model->forget_password($data['email']);
				if($result){
					echo $this->Final_return('success','','');
				}else{
					echo $this->Final_return('error','error','entered email is invalid');	
				}
			}
		}
	}
	public function get_all_speciality(){				       
		$specialties = $this->webservice_model->get_all('specialty_categories');
		echo json_encode($specialties);					
	}
	public function get_all_location(){					      
		$cities = $this->webservice_model->get_all('city_categories');
		echo json_encode($cities);					
	}
	public function get_all_insurance(){				      
		$insurance = $this->webservice_model->get_all('insurance_categories');
		echo json_encode($insurance);					
	}
	public function get_all_reason(){				       
		$visitation = $this->webservice_model->get_all('visit_categories');
		echo json_encode($visitation);					
	}
	public function get_doctors(){
		if($_POST){
			if(isset($_POST) && !empty($_POST)){
				$data =json_decode(file_get_contents('php://input'), true);				
				$result = $this->webservice_model->get_doctors($data);
				echo json_encode($result);
			}
		}		
	}
	public function save_appointment(){
		if(isset(apache_request_headers()['Authorization'])) {
			$data = json_decode(file_get_contents('php://input'), true);
			$auth = apache_request_headers()['Authorization'];
			$result = $this->webservice_model->save_appointment($data,$auth);
			if($result){
				echo $this->Final_return('success','','');
			}else{
				echo $this->Final_return('error','error','entered value is invalid');	
			}
		}	
	}
	public function get_appointment(){		
		$data =json_decode(file_get_contents('php://input'), true);		
		if(isset($data) && !empty($data)){
			$result = $this->webservice_model->get_appointment($data['user_id']);
			$book_info = array();
			foreach ($result as $rs) {
				$count = $this->webservice_model->doctor_count($rs->patient_id,$rs->doctor_id);
				$docInfo = $this->webservice_model->doctor_info($rs->doctor_id);
				//print_r($docInfo);
				$docFullName = trim($docInfo[0]->docFisrtName . ' ' . $docInfo[0]->docLastName);
				$row = array(   'id'=>$rs->id,
								'apnt_starttime'=>$rs->appointment_time,
								'apnt_endtime'=>$rs->end_time,
								'apnt_date'=>$rs->appointment_date,
								'doctor_id'=>$rs->doctor_id,
								'doctor_fullname'=>$docFullName,
								'doctor_email'=>$docInfo[0]->docEmail,
								'doctor_phone'=>$docInfo[0]->docPhone,
								'patient_id'=>$rs->patient_id,
								'illness'=>$rs->ill_reason,
								'apnt_note'=>$rs->apnt_note,
								'status'=>$rs->status,
								'final_status'=>$rs->final_status,
								'apnt_count'=>array('doctor_id'=>$rs->doctor_id,'count'=>$count)
						);
				array_push($book_info, $row);
			}
			if(count($book_info)>0){
				print json_encode(array("status"=>true,'data'=>$book_info));
			} else {
				print json_encode(array("status"=>false,'data'=>array()));
			}
		}		
	}	
	public function get_doctor_info(){	
		$data =json_decode(file_get_contents('php://input'), true);				
		if(isset($data) && !empty($data)){
			$doc_speciality =$this->webservice_model->get_specialties($data['user_id']);
			$doc_details =$this->webservice_model->get_singledoctor($data['user_id']);
			$doc_rating =$this->webservice_model->get_singledoctorrating($data['user_id']);
			$gender = $doc_details->doctor_sex=='male'?'1':'2';				
			$address = $this->webservice_model->get_address($doc_details->cities_id);
			$education = $this->webservice_model->doctor_degree($doc_details->doctor_degree);
			$affiliations = $this->webservice_model->doctor_affiliations($doc_details->doctor_affiliations);
			$dac_array = array("id"=>$doc_details->id,"email"=>$doc_details->email,"password"=>$doc_details->password,"usertype"=>"1","firstname"=>$doc_details->doctor_firstname,"lastname"=>$doc_details->doctor_lastname,"gender"=>$doc_details->$gender,"dob"=>$doc_details->doctor_age,"maritalstatus"=>"","address"=>$doc_details->doctor_office_address.', '.$address->city_name.', '.$address->state_name.', '.$address->country_name.' '.$address->doctor_office_zip,"city"=>$address->city_name,"state"=>$address->state_name,"zipcode"=>$address->doctor_office_zip,"phone"=>$doc_details->phone,"speciality"=>$doc_details->specialty,"description"=>$doc_details->about_doctor,"Education"=>$education,"HospitalAffiliations"=>$affiliations,"BoardCertifications"=>"","ProfessionalMemberships"=>$doc_details->doctor_memberships,"Awards"=>$doc_details->doctor_awards,"working_plan"=>$doc_details->working_time,"breaks"=>$doc_details->break_time,"vecation"=>$doc_details->vacation_time,"createdate"=>"0000-00-00 00=>00=>00","updatedate"=>"0000-00-00 00=>00=>00","secret_key"=>"","status"=>"1");
			if(!empty($doc_details->display_image)):
			$doc_image = $doc_details->display_image;
			else:
			$doc_image = 'uploads/noimage.png';
			endif;
			$data = array(  'doc_rating'=>$doc_rating->avg_rating,
							'doc_details'=>$dac_array,
							'doc_speciality'=>$doc_speciality,
							'doc_image'=>$doc_image
					);
			print json_encode($data);
		}	
	}
	public function get_selection_details(){
		$data =json_decode(file_get_contents('php://input'), true);	
		$data = $data['data'];		
		if(isset($data) && !empty($data)){
			$selected = $data['selectionname'];
			switch ($selected) {
				case 'speciality':
					$table_name='specialty_categories';
					$fields=array('id','specialty_name as name');
					break;
				case 'location':
					$table_name='cities';
					$fields=array('id','city as name');
					break;
				case 'insurance':
					$table_name='insurance_categories';
					$fields=array('id','insurance_name as name');
					break;
			}
			$result = $this->webservice_model->get_selected_data($table_name,$fields);
			echo json_encode($result);
		} else {
			echo json_encode(null);
		}
	}
	public function act_search_bar(){
		$data =json_decode(file_get_contents('php://input'), true);	
		$search_text = $data['search_text'];
		if(is_numeric($search_text)){
			$response['location'] = $this->webservice_model->get_selected_data_like('city_categories',array('city_name as name','id'),array('city_name'),$search_text);
			$response['user'] = $this->webservice_model->get_selected_data_like('doctor',array('id','CONCAT_WS(" ",doctor_firstname,doctor_lastname) AS name'),array('doctor_firstname','doctor_lastname'),$search_text);
		}else{
			$response['location'] = $this->webservice_model->get_selected_data_like('city_categories',array('city_name as name','id'),array('city_name'),$search_text);
			$response['user'] = $this->webservice_model->get_selected_data_like('doctor',array('id','CONCAT_WS(" ",doctor_firstname,doctor_lastname) AS name'),array('doctor_firstname','doctor_lastname'),$search_text);
			$response['speciality'] = $this->webservice_model->get_selected_data_like('specialty_categories',array('id','specialty_name as name'),array('id','specialty_name'),$search_text);
			$response['insurance'] = $this->webservice_model->get_selected_data_like('insurance_categories',array('id','insurance_name as name'),array('id','insurance_name'),$search_text);
			$response['languages'] = $this->webservice_model->get_selected_data_like('language_categories',array('id','language_name as name'),array('id','language_name'),$search_text);
		}
		echo json_encode($response, JSON_FORCE_OBJECT);
	}
	public function get_calendar_details(){	
		$this->load->helper('general');		
		calender();
		$calendar = new Calendar(); 
		echo $calendar->show();
	}
	public function get_search_details(){
		$this->load->helper('general');
		$data =json_decode(file_get_contents('php://input'), true);
		$searchData = $data['searchData'];
		$page     = $searchData['page'];
		$page -= 1;
		$per_page     = 5;
		$start        = $page * $per_page;
		if(!isset($searchData['languages'])){
			$searchData['languages'] = '';
		}
		$resultdata['profile_details'] = $this->webservice_model->searchDoctorLimit($searchData['role'],$searchData['speciality'],$searchData['lat'],$searchData['lng'],$searchData['insurance'],$searchData['languages'],$searchData['gender'],$start,$per_page,0);
		// echo json_encode($resultdata['profile_details']);
		$length = count($this->webservice_model->searchDoctorLimit($searchData['role'],$searchData['speciality'],$searchData['lat'],$searchData['lng'],$searchData['insurance'],$searchData['languages'],$searchData['gender'],$start,$per_page,1));
		// echo json_encode("              ");
		// echo json_encode($length);
		$result_data['length'] =  $length ;
		// echo json_encode("              ");
		// echo json_encode($resultdata['length']);
		$cur_date= date('d-M-Y g:i:s A',strtotime($searchData['selectedDate']));
		// echo json_encode("              ");
		// echo json_encode($cur_date);
		$current_time = strtotime($cur_date);
		// echo json_encode("              ");
		// echo json_encode($current_time);
		// exit();
		$frac = 900;
		$r = $current_time % $frac;
		$new_time = $current_time + ($frac-$r);
		$cur_date = date('d-M-Y g:i:s A', $new_time);
		$status = $searchData['status'];
		// $value['time_details'] = '';
		foreach ($resultdata['profile_details'] as $key => $value) {
			$value['time_details']= init($value['doctorID'],$cur_date,$status);
			$specilaityName = $this->webservice_model->get_specialties($value['doctorID']);
			$value['specilaityName'] = $specilaityName['name'];
			$language = $this->webservice_model->get_languages($value['doctorID']);
			$value['languageName'] = $language['name'];
			// $value['doc_rating'] =$this->webservice_model->get_singledoctorrating($data['user_id']);
			$value['doc_rating'] = $this->webservice_model->get_rating($value['doctorID']);
			$result_data['profile_details'][] = $value;
			// echo json_encode($value['doc_rating']);
			// echo json_encode($resultdata['profile_details']);
			// echo json_encode("               ");

		}	
		// exit();	
		echo json_encode($result_data);
	}
	public function get_doctor_details(){
		$this->load->helper('calender');
		$docData = json_decode(file_get_contents('php://input'), true);
		$docData = $docData['docData'];
		$cur_date= date('d-M-Y g:i:s A',strtotime($docData['date']));
		$status = $docData['status'];
		$doc_details = array();
		$doc_details = init($docData['id'],$cur_date,$status);
		$value['doc_dates'] = $doc_details;
		echo json_encode($value, JSON_FORCE_OBJECT);
		// exit();
		// $doc_details = init(5,'2018-01-10','first');
		// $value['doc_dates']= $doc_details;
		// echo json_encode($value, JSON_FORCE_OBJECT);
	}
	public function get_insurance_list(){
		$this->webservice_model->get_insurance_list();		
	}
	public function visit_categories(){
		$this->webservice_model->get_visit_list();
	}
	public function act_confirm_apnt(){
		$docData = json_decode(file_get_contents('php://input'), true);
		$booking = $docData['Appointment'];
		$booking_reason = $docData['reason'];
		$booking_insurance = $docData['insurance'];
        $apt_date_time = str_replace('T', ' ',$booking['apnttime']);
        list($apt_date,$apt_time) = explode(' ',$apt_date_time);
        $endTime = strtotime("+15 minutes", strtotime($apt_time));
		$endTime = date('h:i A', $endTime);
		$discount_start_date = $apt_date_time;    
		$start_date = date('h:i A', strtotime($discount_start_date));
        $data = array('doctor_id'=>$booking['doctor_id'],
        			  'patient_id'=>$booking['patiendid'],
        			  'appointment_date'=>$apt_date,
        			  'appointment_time'=>$start_date,
        			  'end_time'=>$endTime,
        			  'insurance'=>$booking_insurance,
        			  'ill_reason'=>$booking_reason
				);	
		// echo json_encode($data);
		// exit();
		//die();
		$status = $this->webservice_model->booking_appint($data);
		
		// echo json_encode($x);
		//print_r($status);die();
		// $status = '12';
		// echo json_encode($x);
		// exit();
		if($status){
			// echo "hi";
			//$status = $this->Home_Model->getsendnotification($data);
			print json_encode(array('status'=>true,'bookingid'=>$status));
		
		// $x = $this->Home_Model->sendBookingEmail($data);
		// $z = $this->Home_Model->send_patient_notification($data);

		$y = $this->Home_Model->sendDocBookingEmail($data);
		$a = $this->Home_Model->send_hcp_notification($data);
			
		} else {
			print json_encode(array('status'=>false,'msg'=>"Sorry.This time is booked already.Please try another one"));
		}
	}
	public function cancelbooking(){
		$docData = json_decode(file_get_contents('php://input'), true);
		$booking = $docData['bookingid'];
		$status = $this->webservice_model->cancelbooking($booking);
		if($status){
			$msg = "Deleted Successfully";
		}else{
			$msg = "Sorry!Please try again.";
		}
		print json_encode(array('status'=>$status,'msg'=>$msg));
		// var_dump($booking);
	}
	public function changebookingStat(){
		$docData = json_decode(file_get_contents('php://input'), true);
		$booking_reason = $docData['appointmentid'];
		$booking_insurance = $docData['bookingstatus'];
        $data1 = array(
        			  'final_status'=>$booking_insurance,
        			  'id'=>$booking_reason
				);	
		$status = $this->webservice_model->updateBooking($data1);

		// echo json_encode($status);
		// echo json_encode($data1);
		// exit();

		if($status){
		
			$x = $this->Home_Model->sendBookingEmail($data1);
			$z = $this->Home_Model->send_patient_notification($data1);
			print json_encode(array('status'=>true));
			// echo json_encode($x);

		} else {
			print json_encode(array('status'=>false,'msg'=>"Sorry.This time is booked already.Please try another one"));
		}
	}
	public function getlocation(){
		// echo PHP_VERSION;
		// exit();
		// $id = json_decode(file_get_contents('php://input'),true);
		// if(!empty($id[0])){
			// $x= $id[0]['country'];
			// $rs['data'] =$this->Home_Model->get_cities_id($x);
			$rs['data'] =$this->Home_Model->pull_cities12();
			if($rs['data'] != ''){
				$rs['message'] = "The list of cities";
				$rs['status']	= "Success";
			}else{
				$rs['message'] = "Sorry! We are updating.";
				$rs['status'] = "Failure";
			}
			$rs['responsecode'] = "200";
		// }else{
		// 	// $rs['data'] =$this->Home_Model->get_cityname();
		// 	$rs['message'] = "Please select a city";
		// 	$rs['status']	= "Success";
		// 	$rs['responsecode'] = "200";
		// }
		
		echo json_encode($rs);
		exit();
	}
	public function get_all_degree(){				       
		$degree = $this->Home_Model->get_all_degree();
		echo json_encode($degree);					
	}
	public function get_all_language(){
		$language = $this->Home_Model->get_languag();
		echo json_encode($language);
	}
	public function get_all_country(){
		$country = $this->Home_Model->get_countrylist();
		echo json_encode($country);
	}
	public function updatedoctorprof(){
		$data = array();
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data[0])){
			if(!empty($data[0]['id'])){
				$id = $data[0]['id'];
				$prof = $data[0]['profile_pic'];
				$info = array();
				$info = array(
							'id'=>$data[0]['id'],
					 		'doctor_firstname'=>$data[0]['firstname'],
							'doctor_lastname'=>$data[0]['lastname'],
							'email'=>$data[0]['email'],
							'mdcn'=>$data[0]['mdcn'],
							'doctor_sex'=>$data[0]['gender'],
							'display_image' => $data[0]['profile_pic'],
							// 'cities_id' => $data[0]['location'],
							'lat' => $data[0]['lat'],
							'lon' => $data[0]['lng'],
							'location_str' => $data[0]['location_str'],
							'doctor_degree'=>$data[0]['doctor_degree'],//require id as [1,2]
							'specialty'=>$data[0]['doctor_speciality'],//require id as [1,2] 
							'doctor_languages'=>$data[0]['doctor_language'], //require id as [1,2]
							'doctor_experience'=>$data[0]['doctor_experience'],//require id as [1,2]
							'about_doctor'=>$data[0]['about_doctor'],
							'experience_certificate'=>$data[0]['experience_certificate'],
							'practise_certificate'=>$data[0]['practise_certificate'],
							'phone' => $data[0]['phone'],
							'doctor_office_country' => $data[0]['doctor_office_country'],
							'doctor_office_address' => $data[0]['doctor_office_address'],
							'doctor_office_state' => $data[0]['doctor_office_state'],
							'doctor_office_city' => $data[0]['doctor_office_city'],
							'doctor_office_zip' => $data[0]['doctor_office_zip'],
					);
	
               $result['message'] = $this->Home_Model->updatedoctorprofile($info,$id,$prof);
               $result['Doctor_data'] = $this->Home_Model->getdoctorDetails($id);
               // $result['message'] = $degree;
			}else{
				$result['message'] = 'Please select a doctor!';
			}
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function checksocialemail(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = array(  'email'=>$data['email'],
					);
			$result['message'] = $this->Home_Model->checkdoctorsignup($info);
			}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function checksocialemailpatient(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = array(  'email'=>$data['email'],
					);
			$result['message'] = $this->Home_Model->checkpatientsignup($info);
			}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function doctorfacebooksignup(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = array(  'doctor_firstname'=>$data['firstname'],
							'doctor_lastname'=>$data['lastname'],
							'email'=>$data['email'],
							'master_role_id'=>$data['role_id'],
							'password'=>'',
							'doctor_sex'=>$data['gender'],
							'phone'=>$data['phone'],
							// 'cities_id' => $data['location'],
							'lat' => $data['lat'],
							'lon' => $data['lng'],
							'location_str' => $data['location_str'],
							'terms'=>'agreed',
							'status'=>"1",
					);
			$result['data'] = $this->Home_Model->doctorfbsignup($info);
			$result['message'] = 'Added Successfully';
			}
		else{
			$result['message'] = 'Requested Url needs parameter.';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function patientfacebooksignup(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = array(  'patient_firstname'=>$data['firstname'],
							'patient_lastname'=>$data['lastname'],
							'email'=>$data['email'],
							// 'master_role_id'=>$data['role_id'],
							'password'=>'',
							'patient_sex'=>$data['gender'],
							'phone'=>$data['phone'],
							// 'cities_id' => $data['location'],
							'lat' => $data['lat'],
							'lon' => $data['lng'],
							'location_str' => $data['location_str'],
							'terms'=>'agreed',
							'status'=>"1",
					);
			$result['data'] = $this->Home_Model->patientfbsignup($info);
			$result['message'] = 'Added Successfully';
			}
		else{
			$result['message'] = 'Requested Url needs parameter.';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function doctorgooglelogin(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = array(  'doctor_firstname'=>$data['firstname'],
							'doctor_lastname'=>$data['lastname'],
							'email'=>$data['email'],
							'master_role_id'=>$data['role_id'],
							'password'=>'',
							'doctor_sex'=>$data['gender'],
							'phone'=>$data['phone'],
							// 'cities_id' => $data['location'],
							'lat' => $data['lat'],
							'lon' => $data['lng'],
							'location_str' => $data['location_str'],
							'terms'=>'agreed',
							'status'=>"1",
					);
			$result['data'] = $this->Home_Model->doctorfbsignup($info);
			$result['message'] = 'Added Successfully';
			}
		else{
			$result['message'] = 'Requested Url needs parameter.';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function patientgooglelogin(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			$info = array(  'patient_firstname'=>$data['firstname'],
							'patient_lastname'=>$data['lastname'],
							'email'=>$data['email'],
							// 'master_role_id'=>$data['role_id'],
							'password'=>'',
							'patient_sex'=>$data['gender'],
							'phone'=>$data['phone'],
							// 'cities_id' => $data['location'],
							'lat' => $data['lat'],
							'lon' => $data['lng'],
							'location_str' => $data['location_str'],
							'terms'=>'agreed',
							'status'=>"1",
					);
			$result['data'] = $this->Home_Model->patientfbsignup($info);
			$result['message'] = 'Added Successfully';
			}
		else{
			$result['message'] = 'Requested Url needs parameter.';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function doctorsignup(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			if($data['gender'] == 'Male' || $data['gender'] == 'Female'){
			$info = array(  'doctor_firstname'=>$data['firstname'],
							'doctor_lastname'=>$data['lastname'],
							'email'=>$data['email'],
							'master_role_id'=>$data['role_id'],
							'password'=>md5($data['password']),
							'doctor_sex'=>$data['gender'],
							// 'cities_id' => $data['location'],
							'lat' => $data['lat'],
							'lon' => $data['lng'],
							'location_str' => $data['location_str'],
							'terms'=>'agreed',
							'status'=>"1",
							'mdcn'=>$data['mdcn'],
					);
			$result['message'] = $this->Home_Model->signup($info);
			$result['message1'] = $this->Home_Model->sendEmail($data);
			}else{
				$result['message'] = 'Gender cannot be Null. It should be either Male or Female precisely.';
			}
		}
		else{
			$result['message'] = 'Requested Url needs parameter.';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function doctorlogin(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			// var_dump($data[0]['email']);
			if($data['email'] != ''){
				if($data['password'] != ''){
					$device_id = $data['device_id'];
					$info = array('email'=>$data['email'],
									'password'=>$data['password'],
									);
					$result = $this->Home_Model->doctorlogin($info,$device_id);
				}else{
					$result['message'] = 'Password Cannot be Null!';
				}
			}else{
				$result['message'] = 'Email Cannot be Null!';
			}
		}else{
			$result['message'] = 'Please provide Parameters';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function scheduleWorkPlan(){
		
		$data = json_decode(file_get_contents('php://input'),true);
		$x = json_encode($data['work']);
		// var_dump($x);
		// exit();

		$info = array('working_time'=>$x);
		$id = $data['id'];
		if(!empty($info)){
			$result['message'] = $this->Home_Model->submitWorkPlan($info,$id);
			$result['Doctor_data'] = $this->Home_Model->getdoctorDetails($id);

			$result['status'] = 'Success';
			// $result['message'] = 'Successfully Submitted';

		}else{
			$result['status'] = 'Failure';
			$result['message'] = 'Please Provide some Work plan';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function scheduleBreakPlan(){
		$data = json_decode(file_get_contents('php://input'),true);
		$x = json_encode($data['break']);
		// var_dump($x);
		// exit();

		$info = array('break_time'=>$x);
		$id = $data['id'];
		if(!empty($info)){
			$result['message'] = $this->Home_Model->submitBreakPlan($info,$id);
			$result['status'] = 'Success';
			$result['Doctor_data'] = $this->Home_Model->getdoctorDetails($id);
			// $result['message'] = 'Successfully Submitted';

		}else{
			$result['status'] = 'Failure';
			$result['message'] = 'Please Provide some Break plan';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function scheduleVacationPlan(){
		$data = json_decode(file_get_contents('php://input'),true);
		$x = json_encode($data['vacation']);
		// var_dump($x);
		// exit();

		$info = array('vacation_time'=>$x);
		$id = $data['id'];
		if(!empty($info)){
			$result['message'] = $this->Home_Model->submitVacationPlan($info,$id);
			$result['status'] = 'Success';
			$result['Doctor_data'] = $this->Home_Model->getdoctorDetails($id);
			// $result['message'] = 'Successfully Submitted';

		}else{
			$result['status'] = 'Failure';
			$result['message'] = 'Please Provide some Vacation plan';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function userRole(){
		$data1 = $this->Home_Model->getRoles();
		echo json_encode($data1);
	}
	public function getBookHist(){
		$data = json_decode(file_get_contents('php://input'),true);
		// echo json_encode($data);
		$id = $data['id'];

		$result['responsecode'] = '200';
		if($id){
			$result['history_data'] = $this->Home_Model->getBookHist($id);
			$result['fromtoday'] = $this->Home_Model->getBook($id);
			$result['message'] = 'Booking history details';

			echo json_encode($result);

		}else{
			$result['message'] = 'Sorry Wrong Credentials';
			echo json_encode($result);
		}
	}
	public function getPatientDetails(){
		$data = json_decode(file_get_contents('php://input'),true);
		$id = $data['id'];

		$result['patient_data'] = $this->Home_Model->getPatientDetails($id);
		$result['message'] = 'Patient details';

			echo json_encode($result);
	}
	/*public function sendEmail(){
		$data1 = json_decode(file_get_contents('php://input'),true);
		$data = $data1['email'];
		// echo json_encode($email1);
		$x = $this->Home_Model->sendEmail($data);
		echo json_encode($x);
	}*/
	// public function sendSMS(){
	// 	$x = $this->Home_Model->sendSMS();
	// 	echo json_encode($x);
	// }
	public function getservice(){
		$data = json_decode(file_get_contents('php://input'),true);
		$id = $data['id'];

		$result['Selected_service'] = $this->Home_Model->getService($id);
		$result['service'] = $this->Home_Model->getService12();
		echo json_encode($result);
	} 
	public function updateService(){
		$data = json_decode(file_get_contents('php://input'),true);
		$res['id'] = $data['hcp_id'];
		$res['Selected_service'] = $data['selected_service'];
		$res['new_service'] = $data['new_Service'];

		$result = $this->Home_Model->updateService($res);
		echo json_encode($result);
	}
	public function sendfcm(){
		$result = $this->Home_Model->send_notify();
		echo json_encode($result);
	}
	public function OrganizationSignup(){		
		$data =json_decode(file_get_contents('php://input'), true);
		if(isset($data) && !empty($data)){
			if($this->Isreg_num($data['registration_number'])){
				$data = array('status'=>false,'msg'=>'Sorry This registeration number is existing');
			}else{
				$password = md5($data['password']);	
				$str_passw = $data['password'];
				unset($data['password']);				
				$result = $this->Home_Model->organizationsignup($data,$password);
				if($result){
					$data['password']= $str_passw;
					$x = $this->Home_Model->sendEmail($data);
				unset($data['password']);				
					$data = array('status'=>true,'message'=>'Successfully Organisation Submitted','msg'=>'Successfully Registered. Email sent to your credentials. Please login to your account');
				}
			}
			echo json_encode($data);				
		}		
	}
	public function updateOrganizationprof(){
		$data = array();
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			if(!empty($data['id'])){
				$id = $data['id'];
				$prof = $data['profile_pic'];
				$info = array();
				$info = array(
							'id'=>$data['id'],
					 		'specialty'=>$data['org_speci'],
							// 'email'=>$data['email'],
							'doctor_office_address'=>$data['address'],
							'phone' => $data['phone'],
							'website_link' => $data['website_link'],
							'establish_date' => $data['establish_date'],
							'practise_certificate'=>$data['regst_certi'],
							'experience_certificate'=>$data['cac_registr_certi'],
							'doctor_languages'=>$data['language'],
							'display_image' => $data['profile_pic'],
							'about_doctor'=>$data['message'],
							'mdcn'=>$data['registration_number'],
							'lat' => $data['lat'],
							'lon' => $data['lng'],
							'location_str' => $data['location_str'],
					);
	
               $result['message'] = $this->Home_Model->updateOrganizationprofile($info,$id);
               $result['Organization_data'] = $this->Home_Model->getOrganizationDetails($id);
               // $result['message'] = $degree;
			}else{
				$result['message'] = 'Please select a doctor!';
			}
		}																		
		$result['responsecode'] = '200';
		echo json_encode($result);
	}
	public function organizationlogin(){
		$data =json_decode(file_get_contents('php://input'), true);
		if(!empty($data)){
			// var_dump($data[0]['email']);
			if($data['email'] != ''){
				if($data['password'] != ''){
					// $device_id = $data['device_id'];
					$info = array('email'=>$data['email'],
									'password'=>$data['password'],
									'role_id' => $data['role_id'],
									);
					$result = $this->Home_Model->organizationlogin($info);
				}else{
					$result['message'] = 'Password Cannot be Null!';
				}
			}else{
				$result['message'] = 'Email Cannot be Null!';
			}
		}else{
			$result['message'] = 'Please provide Parameters';
		}
		$result['responsecode'] = '200';
		echo json_encode($result);																																															
	}
	public function Isreg_num($email){
		$doctor_exist = $this->Home_Model->isreg_num($email);	
    if ($doctor_exist) {		
        return false;
		}else {																															
			return true;
		}	
	}
	public function rolespeciality(){
		$data1 = $this->Home_Model->getRoleSpeciality();
		echo json_encode($data1);
	}
	/*public function firebase(){

		$url = "https://fcm.googleapis.com/fcm/send";
		$token = "emD-cVzu_8A:APA91bHZFzLlOWWMxIEtQuc7AvEmVOAkROGmFyQVIzDyEsfhqlzQT7IRsgJIW4I0mTsjr7lhyz-tDySHsjg-jhSqajVIfZLY0bwHAF70ruhKxgDgAyQUHexyQHgnGJo9nNnsFV2XGZlf";
		$serverKey = 'AAAAfU3v14M:APA91bEcJazYoEiAOY1GWY63cHn4iW21vB0P8D2DLKsbn7okUTHeqTuA1Yd59z2aBfhYhef5Xba9-YqOuOmFi7hwjiIOMaWKm5SX3nzAtQySNwAr7UjOGszU_LxlX3lUFsggJAAJg-n2';
		$title = "Title";
		$body = "Body of the message";
		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
		// print_r($arrayToSend);
		$json = json_encode($arrayToSend);
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key='. $serverKey;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
		//Send the request
		$response = curl_exec($ch);
		//Close request
		if ($response === FALSE) {
		die('FCM Send Error: ' . curl_error($ch));
		}
		var_dump($response);
		curl_close($ch);
	}*/
} ?>