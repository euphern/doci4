<?php
class Home_Model extends CI_Model{
	public function _construct(){
		parent::_construct();
	}
	function pull_cities(){
		$query = $this->db->get('cities');
		$result = $query->result();
		return $result;
	}
	function get_languages(){
		$this->db->select('*');
		$this->db->from('language_categories ');
		$this->db->group_by("language_name");       				
		$query = $this->db->get();	
		return $languages = $query->result();
	}
	function get_specialties(){
		$this->db->select('*');
		$this->db->from('specialty_categories ');
		$this->db->group_by("specialty_name");       				
		$query = $this->db->get();	
		return $specialties = $query->result();
	}
	function get_all_degree(){
		$this->db->select('*');
		$this->db->from('degree_categories');
		// $this->db->group_by("specialty_name");       				
		$query = $this->db->get();	
		return $specialties = $query->result();
	}
	function get_insurance(){
		$this->db->select('*');
		$this->db->from('insurance_categories ');
		$this->db->group_by("insurance_name");       				
		$query = $this->db->get();	
		return $insurance = $query->result();
	}
	function pull_footer_cities($limit){
		$this->db->limit($limit);
		$query = $this->db->get('cities');
		$result = $query->result();
		return $result;
	}
	function get_visitation(){
		$this->db->select('*');
		$this->db->from('visit_categories ');
		$this->db->group_by("reason");       				
		$query = $this->db->get();	
		return $visitation = $query->result();
	}
	function get_cityname(){
		$this->db->select('id,city');
		$query = $this->db->get('cities');
		return $result = $query->result();
		// print_r($result);

	}
	function get_countrylist(){
		// echo "hi";
		$this->db->select('*');
		$query = $this->db->get('country_categories');
		return $result = $query->result();
	}
	
	function doctorlogin($info , $device_id){
		// $return_response =array();
		// $uniqueId = $this->generate_unique();
		$query = $this->db->get_where('doctor',array('email'=>$info['email'],'password'=>md5($info['password']),'status'=>'1'));						
		// echo json_encode($query);
		if ($query->num_rows() > 0){
			$result['data'] = $query->result();
			$query1 = $this->db->get_where('doctorfcm',array('doctor_id'=>$result['data'][0]->id));						
			$info1 = array('doctor_id'=>$result['data'][0]->id,
							'fcm_device'=>$device_id,
							);
			if($query1->num_rows() > 0){
				$this->db->where('doctor_id',$result['data'][0]->id);
	    		$res = $this->db->update('doctorfcm',$info1);
			}else{
				
				$this->db->insert('doctorfcm',$info1);
				$data1 = $this->db->insert_id();
			}
			$result['message'] = 'Successfully Logged in';
			// var_dump($result);
	       return $result;
	    }
	    else{
	    	$result['message'] = 'Sorry! Wrong Credentials!';
			// var_dump($result);
	       return $result;
			// return '';
	    }
	}

	function get_countries(){
		$this->db->select('*');
		$this->db->from('country_categories ');
		$this->db->group_by("country_name");       				
		$query = $this->db->get();	
		return $countries = $query->result();
	}
	function get_states(){
		$this->db->select('*');
		$this->db->from('state_categories ');
		$this->db->group_by("state_name");       				
		$query = $this->db->get();	
		return $states = $query->result();
	}
	function get_cities(){
		$this->db->select('*');
		$this->db->from('city_categories ');
		$this->db->group_by("city_name");       				
		$query = $this->db->get();
		return $cities = $query->result();
	}
	function get_languag(){
		$this->db->select('*');
		$this->db->from('language_categories ');
		// $this->db->group_by("city_name");       				
		$query = $this->db->get();
		return $cities = $query->result();
	}	
	function getRoles(){
		$this->db->select('id,role_name');
		$this->db->from('master_role');
		$query = $this->db->get();
		return $cities = $query->result();
	}
	function pull_cities12(){
		$this->db->select('id,city');
		$query = $this->db->get('cities');
		$result = $query->result();
		return $result;
	}
	function get_cities_id($x){
		$query = $this->db->select('id,city')->get_where('cities',array('country' => $x));
		if ($query->num_rows() > 0){
			$result['data'] = $query->result();
			// $result['message'] = 'List of cities';
			// echo json_encode($result);
	       return $result;
	    }
	    // else{
	  //   	// $result['message'] = 'Sorry! No cities found!';
			// // echo json_encode($result);
	  //      return $result;
			// // return '';
	  //   }
	}
	/*function doctorfbsignup(){

	}*/
	function checkdoctorsignup($info){
		$query = $this->db->get_where('doctor',array('email'=>$info['email']));
	    if ($query->num_rows() > 0){
	       return "Existing";
	    }
	    else{
			return  'New';		
	    }
	}
	function checkpatientsignup($info){
		$query = $this->db->get_where('patient',array('email'=>$info['email']));
	    if ($query->num_rows() > 0){
	       return "Existing";
	    }
	    else{
			return  'New';		
	    }
	}
	function doctorfbsignup($info){
		/*echo json_encode($info);
		exit();*/
		$this->db->insert('doctor',$info);
		$data = $this->db->insert_id();
		$info['id'] = $data;
		if(empty($data) || $data == 0){
				return $data;
			}else{
				return $info;		
			}
	}
	function patientfbsignup(){
		$this->db->insert('patient',$info);
		$data = $this->db->insert_id();
		$info['id'] = $data;
		if(empty($data) || $data == 0){
				return $data;
			}else{
				return $info;		
			}
	}
	function signup($info){	    
		// echo json_encode($info);
		// exit();
	    $query = $this->db->get_where('doctor',array('email'=>$info['email']));
	    if ($query->num_rows() > 0){
	       return "Email already Exist!";
	    }
	    else{
	    	$this->db->insert('doctor',$info);
			$data = $this->db->insert_id();
			// $result = $data->result();
			// echo json_encode($data);
			// exit();
			if(empty($data) || $data == 0){
				return 'Sorry! Cannot be Submitted please check!';
			}else{
				return  'Successfully Submitted';		
			}
	    }
	}
	function updatedoctorprofile($info,$id,$prof){
	    	
	$query = $this->db->get_where('doctor',array('id'=>$id));
	    if ($query->num_rows() == 0){
	       $result['message'] = 'Doctor doesnot exists';
	       return $result;
	    }
	    else{
	    	$this->db->where('id',$id);
	    	$query = $this->db->update('doctor',$info);
			if($prof != ''){
				$info1 = array(
					'doctor_id' => $id ,
					'image' => $prof,
					);
			
				$this->db->insert('doctor_gallery',$info1);
				$data = $this->db->insert_id();
				if($query || !empty($data)){
					// $result['message'] = 'Successfully Submitted';
		       		return "Successfully gallery Submitted";
				}else{
					 // $result['message'] = 'Sorry! Cannot be Submitted please check!';
		       		 return 'Sorry! Cannot be gallery Submitted please check!';		
				}
			}else{
				if($query){
		       		return "Successfully Submitted";
				}else{
					 // $result['message'] = 'Sorry! Cannot be Submitted please check!';
		       		 return 'Sorry! Cannot be Submitted please check!';		
				}
			}
	    }		
	}
	
	
	function submitWorkPlan($info,$id){
		$query = $this->db->get_where('doctor',array('id'=>$id));
	    if ($query->num_rows() == 0){
	       $result['message'] = 'Doctor doesnot exists';
	       return $result;
	    }
	    else{
	    	$this->db->where('id',$id);
	    	$query = $this->db->update('doctor',$info);
			if($query){
				// $result['message'] = 'Successfully Submitted';
	       		return "Successfully Submitted";
			}else{
				 // $result['message'] = 'Sorry! Cannot be Submitted please check!';
	       		 return 'Sorry! Cannot be Submitted please check!';		
			}
	    }
	}
	function submitBreakPlan($info,$id){
		$query = $this->db->get_where('doctor',array('id'=>$id));
	    if ($query->num_rows() == 0){
	       $result['message'] = 'Doctor doesnot exists';
	       return $result;
	    }
	    else{
	    	$this->db->where('id',$id);
	    	$query = $this->db->update('doctor',$info);
			if($query){
				// $result['message'] = 'Successfully Submitted';
	       		return "Successfully Submitted";
			}else{
				 // $result['message'] = 'Sorry! Cannot be Submitted please check!';
	       		 return 'Sorry! Cannot be Submitted please check!';	
			}
	    }
	}
	function submitVacationPlan($info,$id){
		$query = $this->db->get_where('doctor',array('id'=>$id));
	    if ($query->num_rows() == 0){
	       $result['message'] = 'Doctor doesnot exists';
	       return $result;
	    }
	    else{
	    	$this->db->where('id',$id);
	    	$query = $this->db->update('doctor',$info);
			if($query){
				// $result['message'] = 'Successfully Submitted';
	       		return "Successfully Submitted";
			}else{
				 // $result['message'] = 'Sorry! Cannot be Submitted please check!';
	       		 return 'Sorry! Cannot be Submitted please check!';
			}
	    }
	}
	function getBookHist($id){
		$this->db->order_by('appointment_date', 'ASC');
		$query =  $this->db->get_where('appointment',array('doctor_id'=>$id));

		$result = $query->result();
		$newarray1 = array();
		foreach ($result as  $value) {
			// echo json_encode($value->patient_id);
			$query1 = $this->db->get_where('patient',array('id'=>$value->patient_id));
			$result1 = $query1->result();
			$newarray['patient_id'] = $result1[0]->id;
			$newarray['patient_name'] =  $result1[0]->patient_firstname.' '.$result1[0]->patient_lastname;
			$newarray['gender'] =  $result1[0]->patient_sex;
			$newarray['appointment_date'] =  $value->appointment_date;
			$newarray['appointment_time'] =  $value->appointment_time;
			$newarray['end_time'] =  $value->end_time;

			array_push($newarray1, $newarray);
		}
		return $newarray1;
	}
	function getBook($id){
		$date = date('Y-m-d');
		$this->db->order_by('appointment_date', 'ASC');
		$query =  $this->db->get_where('appointment',array('doctor_id'=>$id,'appointment_date >='=>$date));
		$result = $query->result();
		$newarray1 = array();
		foreach ($result as  $value) {
			// echo json_encode($value->patient_id);
			$query1 = $this->db->get_where('patient',array('id'=>$value->patient_id));
			$result1 = $query1->result();
			$newarray['patient_id'] = $result1[0]->id;
			$newarray['patient_name'] =  $result1[0]->patient_firstname.' '.$result1[0]->patient_lastname;
			$newarray['gender'] =  $result1[0]->patient_sex;
			$newarray['appointment_date'] =  $value->appointment_date;
			$newarray['appointment_time'] =  $value->appointment_time;
			$newarray['end_time'] =  $value->end_time;
			$newarray['bookingStatus'] =  $value->final_status;
			$newarray['appointmentid'] =  $value->id;

			array_push($newarray1, $newarray);
		}
		return $newarray1;
	}
	function getdoctorDetails($id){
		$query =  $this->db->get_where('doctor',array('id'=>$id));
		$result = $query->result();
		return $result;
	}
	function getPatientDetails($id){
		
	$query =  $this->db->get_where('patient',array('id'=>$id));
		$result = $query->result(); 
		return $result;
	}
	function getsendnotification($data){
		// echo "Hi";
		$query =  $this->db->get_where('doctorfcm',array('doctor_id'=>$data['doctor_id']));
		$result = $query->result(); 
		// $getdeviceid = $this->db->where('doctor_id',$data['doctor_id'])->get('doctorfcm')->num_rows();
// echo json_encode($result[0]->fcm_device);
// exit();
			$fcmApiKey = 'AIzaSyDmOgFISA574v1jkINBQTXUqdxqtHJtAlE';//App API Key(This is google cloud messaging api key not web api key)
		        $url = 'https://fcm.googleapis.com/fcm/send';//Google URL
		 
		    	// $registrationIds = $dataArr['device_id'];//Fcm Device ids array
		 
		    	// $message = $dataArr['message'];//Message which you want to send
		     //    $title = $dataArr['message'];
		 
		        // prepare the bundle
		        $msg = array('message' => 'Booking Successfull on '.$data['appointment_date']." at ".$data['appointment_time'],'title' => Appointment);
		        $fields = array('registration_ids' => $result[0]->fcm_device,'data' => $msg);
		 
		        $headers = array(
		            'Authorization: key=' . $fcmApiKey,
		            'Content-Type: application/json'
		        );
		 
		        $ch = curl_init();
		        curl_setopt( $ch,CURLOPT_URL, $url );
		        curl_setopt( $ch,CURLOPT_POST, true );
		        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		        $result = curl_exec($ch );
		        // Execute post
		        $result = curl_exec($ch);
		        if ($result === FALSE) {
		            die('Curl failed: ' . curl_error($ch));
		        }
		        // Close connection
		        curl_close($ch); 

		        $query =  $this->db->get_where('patientfcm',array('patient_id'=>$data['patient_id']));
		$result = $query->result(); 
		// $getdeviceid = $this->db->where('doctor_id',$data['doctor_id'])->get('doctorfcm')->num_rows();
// echo json_encode($result[0]->fcm_device);
// exit();
			$fcmApiKey = 'AIzaSyDmOgFISA574v1jkINBQTXUqdxqtHJtAlE';//App API Key(This is google cloud messaging api key not web api key)
		        $url = 'https://fcm.googleapis.com/fcm/send';//Google URL
		 
		    	// $registrationIds = $dataArr['device_id'];//Fcm Device ids array
		 
		    	// $message = $dataArr['message'];//Message which you want to send
		     //    $title = $dataArr['message'];
		 
		        // prepare the bundle
		        $msg = array('message' => 'Booking Successfull on '.$data['appointment_date']." at ".$data['appointment_time'],'title' => Appointment);
		        $fields = array('registration_ids' => $result[0]->fcm_device,'data' => $msg);
		 
		        $headers = array(
		            'Authorization: key=' . $fcmApiKey,
		            'Content-Type: application/json'
		        );
		 
		        $ch = curl_init();
		        curl_setopt( $ch,CURLOPT_URL, $url );
		        curl_setopt( $ch,CURLOPT_POST, true );
		        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		        $result = curl_exec($ch );
		        // Execute post
		        $result = curl_exec($ch);
		        if ($result === FALSE) {
		            die('Curl failed: ' . curl_error($ch));
		        }
		        // Close connection
		        curl_close($ch); 
	}
	function sendEmail($data){
		//Load email library
		$this->load->library('email');
		$email = $data['email'];
		$password = $data['password'];

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://smtp.gmail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'docisup@gmail.com',
		    'smtp_pass' => 'ApGf671!',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h1>Registration completed Successfully</h1>';
		$htmlContent .= "<h3>Hi, Your registation  is completed Successfully. Your personal login credentials are mention below.</h3></br><p>Username:- $email<p></br><p>Password:- $password<p></br><p>Thanks and Regards,</p></br><p>Doci Team.</p>";

		$this->email->to($data['email']);
		$this->email->from('docisup@gmail.com','Support Doci');
		$this->email->subject('Thanks For registering at Doci. Please Enjoy our services!');
		$this->email->message($htmlContent);

		//Send email
		$this->email->send();
		return "Email send Successfully";
	}
	// function sendSMS(){
	// 	$this->load->library('twilio');
	// 	$sms_sender = trim($this->input->post('9748237770'));
	// 	$sms_reciever = $this->input->post('8371996353');
	// 	$sms_message = trim($this->input->post('HI acrtive'));
	// 	$from = '+919748237770'/*.$sms_sender;*/ //trial account twilio number
	// 	$to = '+918371996353'/*.$sms_reciever;*/ //sms recipient number
	// 	$message = "HI";
	// 	$response = $this->twilio->sms($from, $to,$message);

	// 	if($response->IsError){

	// 	echo 'Sms Has been Not sent'
	// 	}
	// 	else{

	// 	echo 'Sms Has been sent';
	// 	}
		
	// }
	function  getService($id){
		$this->db->select('*');
	    $this->db->from('Hcp_service');
	    $this->db->join('service', 'Hcp_service.service_id = service.id', 'full outer');
	    $this->db->where('Hcp_service.hcp_id',$id); 
	    $query = $this->db->get();
	    return $query->result();
	}
	function  getService12(){
		$this->db->select('*');
	    $this->db->from('service');
	    // $this->db->join('', 'Hcp_service.service_id = service.id', 'full outer');
	    // $this->db->where('Hcp_service.hcp_id',$id); 
	    $query = $this->db->get();
	    return $query->result();
	}
	function updateService($res){
		// $this->db->
		foreach ($res['Selected_service'] as  $value) {
				$this->db->select('id');
	    		$this->db->from('Hcp_service');
			    $this->db->where('Hcp_service.hcp_id',$res['id']); 
			    $this->db->where('Hcp_service.service_id',$value['sid']); 
			    $query = $this->db->get();
			    $getresult = $query->result();
			    // return $getresult;
			if(!empty($getresult)){
				$arr = array(
							'price'=>$value['price'],
							);
				$this->db->where('id',$getresult[0]->id);
	    		$this->db->update('Hcp_service',$arr);
	    		
			}else{
				$info1 = array(
							'service_id' => $value['sid'] ,
							'price' => $value['price'],
							'hcp_id'=> $res['id'],
							);
		
				// return $info1;
				$this->db->insert('Hcp_service',$info1);
				$data = $this->db->insert_id();
			}
		}
		if(empty($res['new_service'])){
				if($query || !empty($data)){
		       		return "Successfully service Submitted";
				}else{
		       		 return 'Sorry! Cannot be Submitted please check!';		
				}
		}else{
			foreach ($res['new_service'] as  $value) {
					
				$info = array(
							'service_name' => $value['sname'] ,
							);

				$this->db->insert('service',$info);
				$data = $this->db->insert_id();

				$info1 = array(
							'service_id' => $data ,
							'price' => $value['price'],
							'hcp_id'=> $res['id'],
							);
				
				$this->db->insert('Hcp_service',$info1);
				$data = $this->db->insert_id();
			}
			if($query || !empty($data)){
				return "Successfully New service Submitted";
			}else{
				return 'Sorry! Cannot be Submitted please check!';		
			}
		}
	}
	function sendBookingEmail($data1){
		$query2 =  $this->db->get_where('appointment',array('id'=>$data1['id']));
		$result1 = $query2->result();

		$data['doctor_id'] = $result1[0]->doctor_id;
		$data['patient_id'] = $result1[0]->patient_id;
		$data['appointment_date'] = $result1[0]->appointment_date;
		$data['appointment_date'] = $result1[0]->appointment_date;

		$query =  $this->db->get_where('doctor',array('id'=>$data['doctor_id']));
		$result = $query->result();
		// exit();
		if(!empty($result[0])){
			$data['dfname'] = $result[0]->doctor_firstname;
			$data['dlname'] = $result[0]->doctor_lastname;
			$data['demail'] = $result[0]->email;
		}
		$query1 =  $this->db->get_where('patient',array('id'=>$data['patient_id']));
		$result2 = $query1->result();
		if(!empty($result2[0])){
			$data['pfname'] = $result2[0]->patient_firstname;
			$data['plname'] = $result2[0]->patient_lastname;
			$data['pemail'] = $result2[0]->email;
		}
		// return json_encode($data);
		// exit();

if($data1['final_status'] == 'approve'){
	$this->load->library('email');

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://smtp.gmail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'docisup@gmail.com',
		    'smtp_pass' => 'ApGf671!',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);
		$fname = $data['dfname'];
		$lname = $data['dlname'];
		$aptd = $data['appointment_date'];
		$aptt = $data['appointment_time'];
		// $fna= $data['dfname'];
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h1>Appointment Successfully Done.</h1>';
		$htmlContent .= "<h3>Hi, Your appointment has been approved.</h3></br><p>Concerned HCP :-  $fname $lname.<p></br><p>Appointment Date:- $aptd.<p></br><p> Appointment Time:- $aptt.<p></br><p>Thanks and Regards,</p></br><p>Doci Team.</p>";

		$this->email->to($data['pemail']);
		$this->email->from('docisup@gmail.com','Support Doci');
		$this->email->subject('Thanks For appointment booking at Doci. Please Enjoy our services!');
		$this->email->message($htmlContent);

		//Send email
		$this->email->send();
		return "Email send Successfully to patient";
}
if($data1['final_status'] == 'cancel'){
	$this->load->library('email');

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://smtp.gmail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'docisup@gmail.com',
		    'smtp_pass' => 'ApGf671!',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);
		$fname = $data['dfname'];
		$lname = $data['dlname'];
		$aptd = $data['appointment_date'];
		$aptt = $data['appointment_time'];
		// $fna= $data['dfname'];
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h1>Appointment Successfully Done.</h1>';
		$htmlContent .= "<h3>Hi, Your appointment has been cancelled for.</h3></br><p>Concerned HCP :-  $fname $lname.<p></br><p>Appointment Date:- $aptd.<p></br><p> Appointment Time:- $aptt.<p></br><p>Thanks and Regards,</p></br><p>Doci Team.</p>";

		$this->email->to($data['pemail']);
		$this->email->from('docisup@gmail.com','Support Doci');
		$this->email->subject('Thanks For appointment booking at Doci. Please Enjoy our services!');
		$this->email->message($htmlContent);

		//Send email
		$this->email->send();
		return "Email send Successfully to patient";
}		
	}
	function sendDocBookingEmail($data){
		$query =  $this->db->get_where('doctor',array('id'=>$data['doctor_id']));
		$result = $query->result();
		if(!empty($result[0])){
			$data['dfname'] = $result[0]->doctor_firstname;
			$data['dlname'] = $result[0]->doctor_lastname;
			$data['demail'] = $result[0]->email;
		}
		$query1 =  $this->db->get_where('patient',array('id'=>$data['patient_id']));
		$result1 = $query1->result();
		if(!empty($result[0])){
			$data['pfname'] = $result1[0]->patient_firstname;
			$data['plname'] = $result1[0]->patient_lastname;
			$data['pemail'] = $result1[0]->email;
		}

		$this->load->library('email');

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://smtp.gmail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'docisup@gmail.com',
		    'smtp_pass' => 'ApGf671!',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);
		$fname = $data['pfname'];
		$lname = $data['plname'];
		$aptd = $data['appointment_date'];
		$aptt = $data['appointment_time'];
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h1>Appointment Successfully Done.</h1>';
		$htmlContent .= "<h3>Hi, New Appointment received.</h3></br><p>Concerned Patient :- $fname $lname.<p></br><p>Appointment Date :- $aptd.<p></br><p>Appointment Time :- $aptt.<p></br><p>Please Login to your app to confirm the patient booking.</p></br><p>Thanks and Regards,</p></br><p>Doci Team.</p>";

		$this->email->to($data['demail']);
		$this->email->from('docisup@gmail.com','Support Doci');
		$this->email->subject('New Appointment booking received.');
		$this->email->message($htmlContent);

		//Send email
		$this->email->send();
		return "Email send Successfully to doctor";
	}
	function send_patient_notification($data1) {
		$query2 =  $this->db->get_where('appointment',array('id'=>$data1['id']));
		$result1 = $query2->result();

		$data['doctor_id'] = $result1[0]->doctor_id;
		$data['patient_id'] = $result1[0]->patient_id;
		$data['appointment_date'] = $result1[0]->appointment_date;
		$data['appointment_time'] = $result1[0]->appointment_time;
// return $data;
		$this->db->select('*');
		$this->db->from('patient');
	    $this->db->where('patient.id',$data['doctor_id']); 
	    $query = $this->db->get();
	    $getresult = $query->result();

	    $this->db->select('*');
		$this->db->from('doctor');
	    $this->db->where('doctor.id',$data['patient_id']); 
	    $query1 = $this->db->get();
	    $getresult1 = $query1->result();

	    $query3 =  $this->db->get_where('patientfcm',array('patient_id'=>$data['patient_id']));
		$result3 = $query3->result();
		
			    // return $result3[0]->fcm_device;
	    $apptdt = $data['appointment_date'];
	    $appttime = $data['appointment_time'];

			if(!empty($result3[0]->fcm_device) && $result3[0]->fcm_device != 'null'){
					$url = "https://fcm.googleapis.com/fcm/send";
					$token = $result3[0]->fcm_device;
					$serverKey = 'AAAAfU3v14M:APA91bEcJazYoEiAOY1GWY63cHn4iW21vB0P8D2DLKsbn7okUTHeqTuA1Yd59z2aBfhYhef5Xba9-YqOuOmFi7hwjiIOMaWKm5SX3nzAtQySNwAr7UjOGszU_LxlX3lUFsggJAAJg-n2';
					$title = "Doci Booking confirmation";
					$body = "Your booking is confirmed on $apptdt at $appttime";
					$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
					$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
					$json = json_encode($arrayToSend);
					$headers = array();
					$headers[] = 'Content-Type: application/json';
					$headers[] = 'Authorization: key='. $serverKey;
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL,$url);
					curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
					curl_setopt($ch,CURLOPT_POST,true );
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
					curl_setopt($ch,CURLOPT_POSTFIELDS,$json);
					//Send the request
					$response = curl_exec($ch);
					// return $response;
					//Close request
					if ($response === FALSE) {
					die('FCM Send Error: ' . curl_error($ch));
					}
					// var_dump($response);
					// return $response;
					return "Notifications sent";
					curl_close($ch);

			}else{
				return "Device not found";
			}

		}
		function send_hcp_notification($data) {
		$this->db->select('*');
		$this->db->from('patient');
	    $this->db->where('patient.id',$data['doctor_id']); 
	    $query = $this->db->get();
	    $getresult = $query->result();

	    $this->db->select('*');
		$this->db->from('doctor');
	    $this->db->where('doctor.id',$data['patient_id']); 
	    $query1 = $this->db->get();
	    $getresult1 = $query1->result();
			    // return $getresult;
	    $apptdt = $data['appointment_date'];
	    $appttime = $data['appointment_time'];

	    $query3 =  $this->db->get_where('doctorfcm',array('doctor_id'=>$data['doctor_id']));
		$result3 = $query3->result();
// return $result3[0]->fcm_device;
		if(!empty($result3[0]->fcm_device) && $result3[0]->fcm_device != 'null'){
				$url = "https://fcm.googleapis.com/fcm/send";
				$token = $result3[0]->fcm_device;
				$serverKey = 'AAAAdAKsk5w:APA91bFmtLVWHlMrsQNqlanQndHMv1qWYzmyyEXB_5woGdDcoi1dOlGGt2kagi8sfY4-YROWCygtGeHnX6WyqnJm0bYmBIm9In8e-TetDZdfsfuGp6P9yszyUYf_0mX2ZXBXBZ8ffU9L';
				$title = "Doci Booking confirmation";
				$body = "New booking is confirmed on $apptdt at $appttime";
				$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
				$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
				// print_r($arrayToSend);
				$json = json_encode($arrayToSend);
				$headers = array();
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: key='. $serverKey;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);

				/*curl_setopt($ch, CURLOPT_CUSTOMREQUEST,

				"POST");*/
				curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				//Send the request
				$response = curl_exec($ch);
				//Close request
				if ($response === FALSE) {
				die('FCM Send Error: ' . curl_error($ch));
				}
				// var_dump($response);
				curl_close($ch);
		}else{
				return "Device not found";
			}
	}
	function isreg_num($email){
		$query1 = $this->db->get_where('doctor',array('mdcn'=>$email));
		$this->db->last_query();
		if($query1->num_rows()==0){
			return true;
		}else{
			return false;
		}
	}
	function organizationsignup($data,$password){
		/*$info = array(  'organization_name'=>$data['org_name'],
						'organization_specialities'=>$data['org_speci'],
						'master_role_id'=>$data['role_id'],
						'email'=>$data['email'],
						'password'=>$password,
						'address'=>$data['address'],
						'phone'=>$data['phone'],
						'website_link'=>$data['website_link'],
						'establish_date'=>$data['establish_date'],
						'registration_number'=>$data['registration_number'],
						'regst_certi'=>$data['regst_certi'],
						'cac_registr_certi'=>$data['cac_registr_certi'],
						'terms'=>'agreed',
						'status'=>0
				);		*/
				$info = array(  'doctor_firstname'=>$data['org_name'],
						'specialty'=>$data['org_speci'],
						'master_role_id'=>$data['role_id'],
						'email'=>$data['email'],
						'password'=>$password,
						'lat'=>$data['lat'],
						'lon'=>$data['lon'],
						'location_str'=>$data['location_str'],
						'doctor_office_address'=>$data['address'],
						'phone'=>$data['phone'],
						'website_link'=>$data['website_link'],
						'establish_date'=>$data['establish_date'],
						'mdcn'=>$data['registration_number'],
						'practise_certificate'=>$data['regst_certi'],
						'experience_certificate'=>$data['cac_registr_certi'],
						'terms'=>'agreed',
						'status'=>1
				);		
		if($this->db->insert('doctor',$info)){
			return true;
		}
	}
	function updateOrganizationprofile($info,$id){
	    	// return json_encode($info);
	$query = $this->db->get_where('doctor',array('id'=>$id));
	    if ($query->num_rows() == 0){
	       $result['message'] = 'organization doesnot exists';
	       return $result;
	    }
	    else{
	    	$this->db->where('id',$id);
	    	$query = $this->db->update('doctor',$info);
	    	// return $query;
				if($query || !empty($data)){
					// $result['message'] = 'Successfully Submitted';
		       		return "Successfully  Submitted";
				}else{
					 // $result['message'] = 'Sorry! Cannot be Submitted please check!';
		       		 return 'Sorry! Cannot be Submitted please check!';		
				}
	    }		
	}
	function organizationlogin($info){
		// $return_response =array();
		// $uniqueId = $this->generate_unique();
		$query = $this->db->get_where('doctor',array('email'=>$info['email'],'password'=>md5($info['password']),'status'=>'1','master_role_id' => $info['role_id']));						
		// echo json_encode($query);
		if ($query->num_rows() > 0){
			$result['data'] = $query->result();
			// $query1 = $this->db->get_where('doctorfcm',array('doctor_id'=>$result['data'][0]->id));	
			$result['message'] = 'Successfully Logged in';
			// var_dump($result);
	       return $result;
	    }
	    else{
	    	$result['message'] = 'Sorry! Wrong Credentials!';
			// var_dump($result);
	       return $result;
			// return '';
	    }
	}

	function getOrganizationDetails($id){
		$query =  $this->db->get_where('doctor',array('id'=>$id));
		$result = $query->result();
		return $result;
	}
	function getRoleSpeciality(){
		$this->db->select('*');
		$query = $this->db->get('role_speciality');
		return $result = $query->result();
	}
}
?>