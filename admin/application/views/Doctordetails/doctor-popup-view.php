		<div class="row">
            <div class="col-md-6">
              <div class="box box-primary">
				<div class="box-header with-border">
         <h3 class="box-title">View HCP Details</h3>
         <div class="box-tools pull-right">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
            </button>
         </div>
      </div>

                <div class="box-body">
                  <dl>
					  <dt>HCP Firstname</dt>
                      <dd><?php echo $data[0]->doctor_firstname; ?></dd>  
					           
                      <dt>HCP Lastname</dt>
                      <dd><?php echo $data[0]->doctor_lastname; ?></dd>  
					   
					  <dt>Gender</dt>
                      <dd><?php echo $data[0]->doctor_sex; ?></dd> 

                      <dt>Email</dt>
                      <dd><?php echo $data[0]->email; ?></dd> 

                      <dt>Age</dt>
                      <dd><?php echo $data[0]->doctor_age; ?></dd> 

                      <dt>HCP Degree</dt>
                      <dd><?php echo $data[0]->degree_name; ?></dd> 

                      <!-- <dt>HCP Affiliation</dt>
                      <dd><?php echo $data->hospital_name	; ?></dd>  -->

                      <dt>HCP Language</dt>
                      <dd><?php echo $data[0]->doctor_languages; ?></dd> 
                      
                      <!-- <dt>HCP Awards</dt>
                      <dd><?php// echo $data->doctor_awards; ?></dd> 
                      
                      <dt>HCP Membership</dt>
                      <dd><?php// echo $data->doctor_memberships; ?></dd>  -->

                      <dt>HCP Office Address</dt>
                      <dd><?php echo $data[0]->doctor_office_address ; ?></dd> 
					  
					<dt>HCP Office Country</dt>
                      <dd><?php echo $data[0]->country_name ; ?></dd> 
					  

  
                      <dt>HCP Office State</dt>
                      <dd><?php echo $data[0]->state_name ; ?></dd>					  


                      <dt>HCP Office City</dt>
                      <dd><?php echo $data[0]->city_name ; ?></dd> 
 
                  </dl>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- ./col -->
            
            <div class="col-md-6">
              <div class="box box-primary">
               <div class="box-header with-border">
         <h3 class="box-title">View HCP Details</h3>
         <div class="box-tools pull-right">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
            </button>
         </div>
      </div>
                <div class="box-body">
                  <dl>	

                      <dt>HCP Office Zip</dt>
                      <dd><?php echo $data[0]->doctor_office_zip ; ?></dd> 

                      <dt>Specialty</dt>
                      <dd><?php echo $data[0]->specialty; ?></dd> 


                      <dt>Terms</dt>
                      <dd><?php echo $data[0]->terms; ?></dd> 

                      <dt>Status</dt>
                      <dd><?php //echo $data->status; ?></dd> 
					   <?php if($data[0]->status == '1')
			{
			echo "enable";
			}
			else
			{ 
			echo "disable"; 
			}
			?>

                      <!-- <dt>Visitation</dt>
                      <dd><?php echo $data->reason; ?></dd> 

                      <dt>Insurance</dt>
                      <dd><?php echo $data->insurance_name; ?></dd>  -->

                      <dt>Latitude</dt>
                      <dd><?php echo $data[0]->latitude; ?></dd> 


                      <dt>Longitude</dt>
                      <dd><?php echo $data[0]->longitude; ?></dd>
					  
						<!-- <dt>Type</dt>
                      <dd><?php echo $data->type_selection; ?></dd> -->

					 <!--  <?php if(!empty($data->hospital_id)){ ?>
					  <dt>Parent Hospital </dt>
                      <dd><?php echo $res->hospital_name; ?></dd><?php } ?>
					  
					    <?php if(!empty($data->clinic_id)){ ?>
						 <dt>Parent Clinic </dt>
                      <dd><?php echo $res->clinic_name; ?></dd><?php } ?> -->
					  
					   <?php if(!empty($data->medical_id)){ ?>
					    <dt>Parent Medical </dt>
                      <dd><?php echo $res->medical_name; ?></dd><?php } ?>
					  
                      <dt>HCP Experience</dt>
                      <dd><?php echo $data[0]->doctor_experience; ?></dd> 

                      <dt>About HCP</dt>
                      <dd><?php echo $data[0]->about_doctor;?></dd> 

					 <dt>Image</dt>
					  <?php if($data[0]->display_image != NULL && strpos($data[0]->display_image,'assests')){ ?>
					 <img src="<?php echo base_url().$data[0]->display_image; ?>" width="100px" height="100px" alt="Picture Not Found1" />
					  <?php }else if($data[0]->display_image != NULL){?>
            <img src="<?php echo $data[0]->display_image; ?>" width="100px" height="100px" alt="Picture Not Found2" />
              <?php }else{
						 ?>
						 <img src="<?php echo base_url();?>assets/images/user_avatar.jpg" width="100px" height="100px" alt="Picture Not Found3" />
					  <?php } ?>
            <dt>Practise Certificate</dt>
            <?php if($data[0]->practise_certificate != NULL && strpos($data[0]->practise_certificate,'assests')){ ?>
           <img src="<?php echo base_url().$data[0]->practise_certificate; ?>" width="100px" height="100px" alt="Picture Not Found1" />
            <?php }else if($data[0]->practise_certificate != NULL){?>
            <img src="<?php echo $data[0]->practise_certificate; ?>" width="100px" height="100px" alt="Picture Not Found2" />
              <?php }else{
             ?>
             <img src="<?php echo base_url();?>assets/images/user_avatar.jpg" width="100px" height="100px" alt="Picture Not Found3" />
            <?php } ?>
            <dt>Experience Certificate</dt>
            <?php if($data[0]->experience_certificate != NULL && strpos($data[0]->experience_certificate,'assests')){ ?>
           <img src="<?php echo base_url().$data[0]->experience_certificate; ?>" width="100px" height="100px" alt="Picture Not Found1" />
            <?php }else if($data[0]->experience_certificate != NULL){?>
            <img src="<?php echo $data[0]->experience_certificate; ?>" width="100px" height="100px" alt="Picture Not Found2" />
              <?php }else{
             ?>
             <img src="<?php echo base_url();?>assets/images/user_avatar.jpg" width="100px" height="100px" alt="Picture Not Found3" />
            <?php } ?>
                  </dl>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- ./col -->
          </div>  
			
		
		 
		  
		 