<div class="content-wrapper" >
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         HCP Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-user-md"></i>Home</a></li>
         <li class="active"> HCP Details</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title"> View HCP Details</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="" class="table table-bordered table-striped datatable">
                     <thead>
                        <tr>
                           <th class="hidden">ID</th>
                           <th>Serial Number</th>                                                                             
                           <th>HCP Document Image</th>
                           <!-- <th width="">Action</th> -->
                        </tr>
                     </thead> 
                     <tbody>
                        <?php $i =0;
                           foreach($data as $doctors) {	
                           $i++;		 
                           ?>
                        <tr>
                           <td class="hidden"><?php echo $doctors->id; ?></td>
                           <td class="center"><?php echo $i; ?></td>                         
                            <td class="center">
                                 <?php if($doctors->image != NULL && strpos($doctors->image,'assests')){ ?>
                                  <img src="<?php echo base_url().$doctors->image; ?>" width="100px" height="100px" alt="Picture Not Found1" />
                                   <?php }else if($doctors->image != NULL){?>
                                    <img src="<?php echo $doctors->image; ?>" width="100px" height="100px" alt="Picture Not Found2" />
                                <?php }else{
                                     ?>
                                     <img src="<?php echo base_url();?>assets/images/user_avatar.jpg" width="100px" height="100px" alt="Picture Not Found3" />
                                   <?php } ?>
                            </td> 
						   				                                                                                            
                        <!--    <td class="center">	                         
                              <a class="btn btn-sm bg-olive show-doctordetails"  href="javascript:void(0);"  data-id="<?php echo $doctors->id; ?>">
                              <i class="fa fa-fw fa-eye"></i> View </a>    
                              <a class="btn btn-sm bg-olive"  href=""<?php echo base_url();?>Doctor_ctrl/documents/<?php echo $doctors->id; ?>">
                              <i class="fa fa-fw fa-book"></i> Documents </a> 
                              <a class="btn btn-sm btn-danger" href="<?php echo base_url();?>Doctor_ctrl/delete_doctor/<?php echo $doctors->id; ?>" onClick="return doconfirm()">
                              <i class="fa fa-fw fa-trash"></i>Delete</a>	
							 <?php if( $doctors->status){?>
                              <a class="btn btn-sm label-warning" href="<?php echo base_url();?>Doctor_ctrl/doctor_status/<?php echo $doctors->id; ?>"> 
                              <i class="fa fa-folder-open"></i> Disable </a>           
                              <?php
                                 }
                                 else
                                 {
                                 ?>
                              <a class="btn btn-sm label-success" href="<?php echo base_url();?>Doctor_ctrl/doctor_active/<?php echo $doctors->id; ?>"> 
                              <i class="fa fa-folder-o"></i> Enable </a>
                              <?php
                                 }
                                 ?>
                           </td> -->
                        </tr>
                        <?php
                           }
                           ?>
                     </tbody>
                     <tfoot>
                        <tr>
                            <th class="hidden">ID</th>
                           <th>HCP Firstnam</th>                                                                    
                           <th>HCP Lastname</th>						   
                           <th width="">Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
